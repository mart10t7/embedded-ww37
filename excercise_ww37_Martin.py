#Libraries
import RPi.GPIO as GPIO
from time import sleep
#Set warnings off (optional)
GPIO.setwarnings(False)
GPIO.setmode(GPIO.BCM)
#Set Button and LED pins
Button = 23
LED = 24
#Setup Button and LED
GPIO.setup(Button,GPIO.IN,pull_up_down=GPIO.PUD_UP)
GPIO.setup(LED,GPIO.OUT)
flag = 0

while True:
    button_state = GPIO.input(Button) # Button input
    if button_state == 0:
	if flag == 0:
		GPIO.output(LED,True)
		flag=1
		sleep(.5)
	else:
		GPIO.output(LED,False)
		flag=0
		sleep(1)
